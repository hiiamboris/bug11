# bug11

- clone everything
- run bug11-hunt.bat; wait a minute or so - it builds and runs `bug11.red` script with different iteration counter values
- grep the log for "error"

Common errors that pop up:
```
*** Script Error: PARSE - matching by datatype not supported for any-string! input
*** Where: parse
*** Stack: view show show do-safe react
```
```
*** Script Error: PARSE - matching by datatype not supported for any-string! input
*** Where: parse
*** Stack: view show show do-safe error? on-face-deep-change* show show do-safe react
```
```
*** Runtime Error 1: access violation
*** at: 00427F74h
```
```
*** Runtime Error 1: access violation
*** Cannot determine source file/line info.
***
```