Red [needs: view]

clock: function [c] [
	print ["0 stats=" stats]
	t1: now/time/precise r: do c t2: now/time/precise probe (1e3 * third to time! t2 - t1 // 24:0:0) :r
]

system/view/VID/styles/text-list: [
	; default-actor: on-change
	init: [
		face/internal-actors: object face/internal-actors
		face/actors: make face/user-actors: any [face/actors object []] collect [
			foreach w words-of face/internal-actors [
				keep compose/deep [
					(to set-word! w) func [fa ev /local f] [
						f: select fa/internal-actors quote (w)  f fa ev
						if f: select fa/user-actors quote (w)  [f fa ev]
					]
				]
			]
		]
	]

	template: [

		type: 'rich-text
		color: system/view/metrics/colors/window
		para: make para! [align: 'left v-align: 'top]
		font: make font! bind [name: fonts/system size: fonts/size] system/view
		size: 150x150
		; flags: 'all-over
		text: none
		data: []

		tabs: none line-spacing: 'default handles: none

		; wheel-outside?: no
		top: left: 0
		line-height: 1.0
		viewport: size
		hscroller: vscroller: none

		dim-calc: function [data [block! none!]][
			unless block? data [return 0x0]
			rt: rtd-layout reduce [""]
			rt/font: all [font  make font [parent: state: none]]
			rt/size: none
			rt/options: :options
			rt/text: rejoin parse data [collect any [keep string! keep (lf) | skip]]
			size-text rt
		]
		canvas: is [clock [dim-calc data]]
		width: is [canvas/x]
		length: is [canvas/y]

		maybe: func ['sym val] [
			sym: either word? sym [to word! sym][to path! sym]
			unless strict-equal? get sym :val [
				set sym :val
			]
			:val
		]
		offset-to-line: func [fa of] [
			of: of + as-pair fa/left fa/top
			1 + to integer! round/floor of/y / fa/line-height
		]
		select-line: function [fa ev ln] [
			ev': object collect [
				foreach w system/catalog/accessors/event! [
					keep compose [(to set-word! w) (to path! reduce ['ev w])]
				]
				keep compose [picked: (ln)]
			]
			unless all [
				act: select fa/actors 'on-select
				'stop = act fa make ev' [type: 'select]
			][
				fa/maybe fa/selected: ln
				if act: select fa/actors 'on-change [
					r: act fa make ev' [type: 'change]
				]
			]
			:r
		]

		user-actors: none
		internal-actors: [

			on-created: function [fa] [
				print ["1 stats=" stats]
				append fa/parent/pane vsc: fa/vscroller: make-face/size 'scroller 16x150
				append fa/parent/pane hsc: fa/hscroller: make-face/size 'scroller 150x16

				react/link/later function [fa _] [
					print ["2 stats=" stats]
					?: :fa/maybe
					vp: fa/viewport

					if empty? lfs: "" [append/dup lfs lf 99]
					text: in fa 'text
					set-quiet in fa 'text lfs
					? fa/line-height: lh: 0.01 * second size-text fa
					set-quiet in fa 'text :text

					rt: rtd-layout reduce [""]
					rt/font: all [fa/font  make fa/font [parent: state: none]]
					rt/size: none
					foreach w [color image flags options edge para] [rt/:w: :fa/:w]

					t: copy ""  ysz: 0.0  xsz: 0.0
					collect/into [attempt [
						foreach s fa/data [
							unless string? :s [continue]
							ysz: ysz + lh
							if ysz <= fa/top [continue]
							keep :s keep lf 
							if all [
								get 'keep
								ysz >= (fa/top + vp/y)
							] [set 'keep none]
						]
					]] t
					if lf = last t [take/last t]
					? fa/length: to integer! ysz 

					c: any [all [fa/font fa/font/color] system/view/metrics/colors/text]
					rt/text: t
					rt/data: reduce [as-pair 1 length? t c]

					where: as-pair  0 - fa/left  0 - fa/top % lh
					selbox: []
					if integer? :fa/selected [
						sel1: as-pair  0 - fa/left  fa/selected - 1 * lh - fa/top
						sel2: sel1 + as-pair  -1 + max fa/width vp/x  lh
						if all [
							sel2/x >= 0    sel2/y >= 0
							sel1/x < vp/x  sel1/y < vp/y
						][
							chl: do bind [colors/text / 2 + (colors/window / 2) + 0.0.0.127] system/view/metrics
							selbox: reduce ['pen c 'fill-pen chl 'box sel1 sel2]
						]
					]
					fa/draw: compose [clip 0x0 (vp) text (where) (rt) (selbox)]
				] [fa fa]
				fa/data: fa/data

				react/link/later function [fa vsc hsc] [
					print ["3 stats=" stats]
					?: :fa/maybe
					unless fa/visible? [
						? vsc/visible?: ? hsc/visible?: no
						exit
					]

					clip: func [x m] [max 0.0 min m x]
					vsz: fsz: fa/size

					if ? vsc/visible?: fa/length > vsz/y [vsz/x: vsz/x - vsc/size/x]
					if ? hsc/visible?: fa/width > vsz/x [
						vsz/y: vsz/y - hsc/size/y
						if all [
							vsz/x = fsz/x
							? vsc/visible?: fa/length > vsz/y
						] [vsz/x: vsz/x - vsc/size/x]
					]
					? fa/viewport: vsz
					? vsc/steps: max 0.01 1.0 * fa/line-height / fa/length

					? vsc/size/y: vsz/y
					? hsc/size/x: vsz/x
					? vsc/offset: fsz - vsc/size * 1x0 + fa/offset
					? hsc/offset: fsz - hsc/size * 0x1 + fa/offset

					sel: clip  1.0 * vsz/y / fa/length  1.0
					? vsc/selected: 100% * round/to/floor sel 0.01
					? fa/top: to integer! fa/length * clip vsc/data 1.0 - sel
					sel: clip  1.0 * vsz/x / fa/width  1.0
					sel: ? hsc/selected: 100% * round/to sel 0.01
					? fa/left: to integer! fa/width * clip hsc/data 1.0 - sel
				] [fa vsc hsc]
				fa/visible?: fa/visible?
			]

		]
	]
]

d: collect [repeat i 1000 [keep append/dup copy "" form i 15]]
d2: collect [repeat i 100 [keep append/dup copy "" form i 1]] 
probe stats
view [
	t: text-list 100x500 with [data: d]
	t2: text-list 200x500 with [data: d2]
	f: field on-change [t/data: t/data] rate 10 on-time [quit]
]
quit